package com.Mrbysco.UnderPuzzle;

public class PuzzleReference {
	public static final String MOD_ID = "underpuzzle";
	public static final String MOD_NAME = "Undertale Puzzle";
	public static final String VERSION = "1.0.0";
	public static final String CLIENT_PROXY_CLASS = "com.Mrbysco.UnderPuzzle.proxy.ClientProxy";
	public static final String SERVER_PROXY_CLASS = "com.Mrbysco.UnderPuzzle.proxy.CommonProxy";
}
